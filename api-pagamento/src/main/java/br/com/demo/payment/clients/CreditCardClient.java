package br.com.demo.payment.clients;

import br.com.demo.payment.models.dto.CreditCardResponse;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(name = "cartao")
public interface CreditCardClient {

        @GetMapping("cartao/id/{id}")
        CreditCardResponse getCartaoById(@PathVariable Long id);

}
