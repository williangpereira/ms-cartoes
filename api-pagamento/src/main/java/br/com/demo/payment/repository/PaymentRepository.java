package br.com.demo.payment.repository;


import br.com.demo.payment.models.Payment;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface PaymentRepository extends CrudRepository<Payment, Long> {

    List<Payment> findAllByCreditCardId(Long creditCardId);
}
