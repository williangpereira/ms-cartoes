package br.com.demo.creditcard.clients;

import br.com.demo.creditcard.model.dto.CustomerDTO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(name = "cliente")
public interface CustomerClient {

    @GetMapping("/cliente/{id}")
    CustomerDTO getCustomerById(@PathVariable Long id);

}
