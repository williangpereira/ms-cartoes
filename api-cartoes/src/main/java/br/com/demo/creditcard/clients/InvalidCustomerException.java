package br.com.demo.creditcard.clients;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.BAD_REQUEST, reason = "O cliente informado não é valido!")
public class InvalidCustomerException extends RuntimeException{
}
